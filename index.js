function countLetter(letter, sentence) {
    let result = 0;

    if(letter.length === 1) {
        for(let i=0; i < sentence.length; i++) {
            if(sentence[i].toLowerCase() === letter.toLowerCase()) {
                result++;
            }
        }
        return result;
    }
    else {
        return undefined;
    }
}

function isIsogram(text) {
    let { length } = text;

    for(let i=0; i < length; i++) {
        let letter = text[i].toLowerCase();
        for(let z = i+1; z < length; z++) {
            if(letter === text[z].toLowerCase()){
                return false;
            }
        }
    }
    return true;
}

function purchase(age, price) {
    if((age >= 13 && age <= 21) || age >= 65) {
        return (price * .80).toFixed(2).toString();
    }
    else if (age >= 22 && age <= 64) {
        return price.toFixed(2).toString();
    }
    else {
        return undefined;
    }
}

function findHotCategories(items) {
    let result = [];

    items.map(item => {
        if(item.stocks === 0 && !result.includes(item.category)) {
            result.push(item.category)
        }
    })
    return result;
}

function findFlyingVoters(candidateA, candidateB) {
    return candidateB.concat(candidateA).filter((value, index, self)  =>  {
        return (self.indexOf(value) !== index)
    })  
}

module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};